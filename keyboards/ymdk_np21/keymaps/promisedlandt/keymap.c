#include QMK_KEYBOARD_H

enum layer_names {
    _DEVELOPMENT,
    _NUMPAD,
    _BL,
    _GIT,
    _WINDOW,
};

enum custom_keycodes {
    GIT_STATUS = SAFE_RANGE,
    GIT_CO_LAST,
    GIT_ADD,
    GIT_CO_MASTER,
    GIT_CO,
    GIT_DIFF,
    GIT_DIFF_CACHED,
    GIT_BASE,
    WINDOW_GAMES,
    WINDOW_COMS,
    WINDOW_NOTES,
    WINDOW_MUSIC,
    DEV_NOTES_PRIVATE,
    DEV_NOTES_WOW,
    DEV_NOTES_WORK,
    DEV_CAPTURE_PRIVATE,
    DEV_CAPTURE_WOW,
    DEV_CAPTURE_WORK,
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case DEV_NOTES_PRIVATE:
            if (record->event.pressed) {
                SEND_STRING(SS_LALT(SS_LWIN(SS_TAP(X_F10))));
            }
            break;
        case DEV_NOTES_WOW:
            if (record->event.pressed) {
                SEND_STRING(SS_LALT(SS_LWIN(SS_TAP(X_F11))));
            }
            break;
        case DEV_NOTES_WORK:
            if (record->event.pressed) {
                SEND_STRING(SS_LALT(SS_LWIN(SS_TAP(X_F12))));
            }
            break;
        case DEV_CAPTURE_PRIVATE:
            if (record->event.pressed) {
                SEND_STRING(SS_LALT(SS_LWIN(SS_TAP(X_F10))) SS_DELAY(50) SS_TAP(X_SPACE) "e");
            }
            break;
        case DEV_CAPTURE_WOW:
            if (record->event.pressed) {
                SEND_STRING(SS_LALT(SS_LWIN(SS_TAP(X_F11))) SS_DELAY(50) SS_TAP(X_SPACE) "e");
            }
            break;
        case DEV_CAPTURE_WORK:
            if (record->event.pressed) {
                SEND_STRING(SS_LALT(SS_LWIN(SS_TAP(X_F12))) SS_DELAY(50) SS_TAP(X_SPACE) "e");
            }
            break;
        case GIT_STATUS:
            if (record->event.pressed) {
                SEND_STRING("git status"SS_TAP(X_ENTER));
            }
            break;
        case GIT_CO_LAST:
            if (record->event.pressed) {
                SEND_STRING("git checkout -"SS_TAP(X_ENTER));
            }
            break;
        case GIT_ADD:
            if (record->event.pressed) {
                SEND_STRING("git commit --all --message=\"\""SS_TAP(X_LEFT));
            }
            break;
        case GIT_CO_MASTER:
            if (record->event.pressed) {
                SEND_STRING("git checkout master"SS_TAP(X_ENTER));
            }
            break;
        case GIT_CO:
            if (record->event.pressed) {
                SEND_STRING("git checkout ");
            }
            break;
        case GIT_DIFF:
            if (record->event.pressed) {
                SEND_STRING("git diff"SS_TAP(X_ENTER));
            }
            break;
        case GIT_DIFF_CACHED:
            if (record->event.pressed) {
                SEND_STRING("git diff --cached"SS_TAP(X_ENTER));
            }
            break;
        case GIT_BASE:
            if (record->event.pressed) {
                SEND_STRING("git ");
            }
            break;
        case WINDOW_GAMES:
            if (record->event.pressed) {
                SEND_STRING(SS_LGUI("w") SS_DELAY(50) "g");
            }
            break;
        case WINDOW_COMS:
            if (record->event.pressed) {
                SEND_STRING(SS_LGUI("w") SS_DELAY(50) "c");
            }
            break;
        case WINDOW_NOTES:
            if (record->event.pressed) {
                SEND_STRING(SS_LGUI("w") SS_DELAY(50) "n");
            }
            break;
        case WINDOW_MUSIC:
            if (record->event.pressed) {
                SEND_STRING(SS_LGUI("w") SS_DELAY(50) "b");
            }
            break;
    }

    return true;
};


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_DEVELOPMENT] = LAYOUT_ortho_6x4(
            DF(_DEVELOPMENT),  KC_NO,  KC_NO, MO(_BL),
            TG(_NUMPAD),  KC_PSCREEN,  LCA(KC_SPACE), LCMD(KC_L),
            KC_NO,  KC_NO,  KC_NO, KC_NO,
            KC_NO,  KC_NO,  KC_NO, KC_NO,
            OSL(_GIT),  DEV_CAPTURE_PRIVATE,  DEV_CAPTURE_WOW, DEV_CAPTURE_WORK,
            OSL(_WINDOW),  DEV_NOTES_PRIVATE,  DEV_NOTES_WOW, DEV_NOTES_WORK
            ),

    [_NUMPAD] = LAYOUT_ortho_6x4(
            KC_ESC,  KC_TAB,  KC_BSPC, MO(_BL),
            TO(_DEVELOPMENT), KC_PSLS, KC_PAST, KC_PMNS,
            KC_P7,   KC_P8,   KC_P9,   KC_PPLS,
            KC_P4,   KC_P5,   KC_P6,   KC_PPLS,
            KC_P1,   KC_P2,   KC_P3,   KC_PENT,
            KC_P0,   KC_DOT,  KC_PDOT, KC_PENT
            ),

    [_BL] = LAYOUT_ortho_6x4(
            _______, _______, _______, _______,
            _______, _______, _______, _______,
            _______, BL_ON,   _______, BL_INC,
            _______, BL_TOGG, _______, BL_INC,
            _______, BL_OFF,  _______, BL_DEC,
            BL_BRTG, _______, _______, BL_DEC
            ),

    [_GIT] = LAYOUT_ortho_6x4(
            _______, _______, _______, _______,
            _______, _______, _______, _______,
            _______, _______, _______, _______,
            GIT_ADD, GIT_DIFF, GIT_DIFF_CACHED, GIT_STATUS,
            GIT_CO_LAST, GIT_CO_MASTER, GIT_CO, TG(_GIT),
            GIT_BASE, _______, _______, TG(_GIT)
            ),

    [_WINDOW] = LAYOUT_ortho_6x4(
            _______, _______, _______, _______,
            _______, _______, _______, _______,
            _______, _______, _______, _______,
            WINDOW_COMS, LGUI(KC_1), WINDOW_GAMES, _______,
            LGUI(KC_3), WINDOW_NOTES, WINDOW_MUSIC, TG(_WINDOW),
            LSFT(KC_BSPACE), LGUI(KC_2), _______, TG(_WINDOW)
            )
};
